resource "hcloud_server" "node1" {
  name        = "hetznernode1"
  image       = var.ubuntu_version
  server_type = "cx11"
  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }
  ssh_keys = ["boris", "yannis"]

}
